$(function() {

    "use stric";
    // Scroll Top 
    var scrollButton = $("#scroll-top");
    scrollButton.click(function() {
        $("html,body").animate({
            scrollTop: 0
        }, 1500);
    });
    $(window).scroll(function() {
        if (scrollButton.length) {
            if ($(this).scrollTop() >= 200) {
                scrollButton.show()
            } else {
                scrollButton.hide()
            }
        }
    });

    // smooth scroll to div 
    var nav = $(".navbar.navbar-default");
    if ($('.navs li a,.animationTop a, .angleDown').length) {
        $('.navs li a,.animationTop a, .angleDown').click(function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: $('#' + $(this).data('value')).offset().top
            }, 1000);
        });
    }

    // remove startLoading
    $('.startLoading ').click(function() {
        $('.startLoading ').slideUp(300);
        $('.headerNav').addClass('curtain');
        $('.headerNav .container').removeClass('mainContainer')
    });


    // $(".mobileSlider").owlCarousel({
    //     items: 1,
    //     autoplay: 4000,
    //     loop: true,
    //     nav: true,
    //     navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    // });

    $(".servicesMobile").owlCarousel({
        items: 1,
        autoplay: false,
        loop: false,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-arrow-right'></i>"]
    });
});

// loading
window.onload = function() {
    $('#page-preloader').delay(200).fadeOut(500);
}